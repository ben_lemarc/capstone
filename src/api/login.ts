import { LoginEntity } from "../model/login";
import axios from 'axios';
import { createEmptyUser, UserEntity, LoggedInUser } from '../model';
import { getUserEntityFromResponse } from '../api/user'

// Just a fake loginAPI
export const isValidLogin = (loginInfo: LoginEntity): Promise<UserEntity> => 
    new Promise((resolve, reject) => {
        axios.get('/sanctum/csrf-cookie').then(first_response => {
            axios.post('/login', {
                email: loginInfo.email,
                password: loginInfo.password
            })
            .then(second_response => {
                console.log(second_response);
                const user: UserEntity = getUserEntityFromResponse(second_response.data.data);
                console.log(user);
                resolve(user);
            }, (error) => {
                reject(error.response.data.data);
            })
        });
    });