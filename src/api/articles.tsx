import axios from 'axios';
import { ArticleEntity, createEmptyArticle, ArticlesResponse, ArticleRecord } from "../model";

export const getArticles = (): Promise<ArticleEntity[]> =>
    new Promise((resolve, reject) => {
        axios.get('/articles').then(response => {
            resolve(getArticlesFromResponse(response.data));
        }, (error => {
            reject(error.response);
        }));
    });

export const getArticlesFromResponse = (response: ArticlesResponse): ArticleEntity[] => {
    const articles: ArticleEntity[] = response.data.map( (articleJSON: ArticleRecord) => {
            let newArticle: ArticleEntity = getArticleEntityFromResponse(articleJSON);
            return newArticle;
        });
    return articles;
};

const getArticleEntityFromResponse = (record: ArticleRecord): ArticleEntity => {
    let article: ArticleEntity = createEmptyArticle();
    article.id = record.id;
    article.title = record.title;
    article.author = record.author;
    article.body = record.body;
    article.articleImageUrl = record.article_image_url;
    article.authorImageUrl = record.author_image_url;
    article.publishedAt = record.published_at;
    article.trending = record.trending;
    return article;
};