import React, {useContext} from "react";
import axios from 'axios';
import { createEmptyUser } from "../model";
import {AuthContext} from "../context/AuthContext";

export const logout = (): Promise<boolean> =>
    new Promise((resolve, reject) => {
        axios.post('/logout').then(response => {
            resolve(true);
        }, (error) => {
            reject(error.response);
        });
    });