import axios from 'axios';
import { UserEntity, createEmptyUser, paginate, UserRecord, UsersResponse } from "../model";

export const getUserFromAPI = (): Promise<UserEntity> =>
    new Promise((resolve, reject) => {
        axios.get('/api/user').then(response => {
            let user: UserEntity = getUserEntityFromResponse(response.data);
            resolve(user);
        }, (error => {
            reject(error.response);
        }));
    });

export const getUserEntityFromResponse = (data:UserRecord): UserEntity => {
    let user: UserEntity = createEmptyUser();
    user.firstName = data.first_name;
    user.lastName = data.last_name;
    user.title = data.title;
    user.email = data.email;
    user.id = data.id;
    user.role = data.role;
    user.createdAt = data.created_at
    user.phoneNumber = data.phone_number;
    user.notes = data.notes;
    user.fullTime = data.full_time;
    return user;
}

export const deleteUser = (userId:number): Promise<boolean> =>
    new Promise((resolve, reject) => {
        axios.delete(`/users/${userId}`).then(response => {
            resolve(true);
        }, (error => {
            reject(error.response);
        }));
    });

export const getUsers = (): Promise<UserEntity[]> =>
    new Promise((resolve, reject) => {
        axios.get('/users').then(response => {
            resolve(getUsersFromResponse(response.data));
        }, (error => {
            reject(error.response);
        }));
    });

export const getUsersFromResponse = (response: UsersResponse): UserEntity[] => {
    const users: UserEntity[] = response.data.map( (userJSON:UserRecord) => {
        let newUser:UserEntity = getUserEntityFromResponse(userJSON);
            return newUser;
        });
    return users;
}

export const getUserView = (id:number): Promise<UserEntity> => 
    new Promise((resolve, reject) => {
        axios.get(`users/${id}`).then(response => {
            console.log(response);
            let user: UserEntity = getUserEntityFromResponse(response.data.data);
            resolve(user);        
        }, (error => {
            reject(error.response);
        }));
    });

export const createUser = (newUser: UserEntity): Promise<boolean> => 
    new Promise((resolve, reject)=> {
        axios.post('/users', {
                first_name: newUser.firstName,
                last_name: newUser.lastName,
                title: newUser.title,
                email: newUser.email,
                role: newUser.role,
                phone_number : newUser.phoneNumber,
                notes : newUser.notes,
                full_time : newUser.fullTime,
            })
            .then(response => {
                resolve(true);
            }, (error) => {
                reject(error.response);
            });
    });

export const updateUser = (updatedUser: UserEntity): Promise<boolean> =>
    new Promise((resolve, reject) => {
        axios.put(`/users/${updatedUser.id}`, {
            first_name: updatedUser.firstName,
            last_name: updatedUser.lastName,
            title: updatedUser.title,
            email: updatedUser.email,
            role: updatedUser.role,
            phone_number : updatedUser.phoneNumber,
            notes : updatedUser.notes,
            full_time : updatedUser.fullTime,
        })
        .then(response => {
            resolve(true);
        }, (error) => {
            reject(error.response);
        })
});

