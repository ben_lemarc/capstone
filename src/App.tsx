import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import { HashRouter, Switch, Route } from 'react-router-dom';
import {Home, Login, Users, UserViewAuth, NewUser, News} from './pages';
import { Header, Footer } from "./components";
import { UserEntity, createEmptyUser, ADMIN } from './model';
import {AuthContext} from "./context/AuthContext"
import {StorageHelper} from "./helpers/storageHelper";
import {AuthRoute} from "./routes/authRoute";

function App() {

    const [user, setUser] = useState(StorageHelper.getUser());

    const updateUser = (user: UserEntity|null)=> {
        StorageHelper.setUser(user);
        setUser(user);
    };

    const isAdmin = () => {
        return user?.role === ADMIN;
    };

    return (
        <>
            <AuthContext.Provider value={{ user:user, setUser:updateUser,  isAdmin:isAdmin}}>
                <HashRouter>
                    <Header/>
                    <Switch>
                        <AuthRoute path="/news" component={News}/>
                        <Route exact={true} path="/" component={Login}/>
                        <Route path="/login" component={Login} />
                        <AuthRoute path={"/users/:userId"} component={UserViewAuth} />
                        <AuthRoute path="/home" component={Users} />
                        <AuthRoute path="/newUser" component={NewUser} />
                    </Switch>
                    <Footer/>
                </HashRouter>
            </AuthContext.Provider> 
        </>
        );
    };

    export default App;

axios.defaults.baseURL = 'http://api.capstone.test/';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.withCredentials = true;

