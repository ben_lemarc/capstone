import { createContext } from 'react';
import {UserEntity, createEmptyUser} from "../model";

export interface IAuthContext {
    user: UserEntity|null;
    setUser: (user: UserEntity|null) => void;
    isAdmin: () => boolean;
}

export const AuthContext = createContext<IAuthContext>({
    user: null,
    setUser: () => {},
    isAdmin: () => false
});