import React, { useState, useEffect, FC, useContext, ChangeEvent } from "react"
import { getUsers } from "../api"
import { UserEntity, createEmptyUser, paginate, ErrorResponse, KeyboardInputEvent } from "../model";
import { UserTable } from "../components"
import { Link } from 'react-router-dom'
import { useHistory } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { formatDate } from "../helpers/dateHelper";
import "../css/userTable.css";

export const Users: FC = () => {
    
    const [page, setPage] = useState<number|null>(null);
    const [errorMessage, setErrorMessage] = useState<string|null>(null);
    const [users, setUsers] = useState<UserEntity[]>([]);
    const [filteredUsers, setFilteredUsers] = useState<UserEntity[][]>([]);
    const [searchTerm, setSearchTerm] = useState<string>("");

    const auth = useContext(AuthContext);
    const usersPerPage: number = 10;
    const history = useHistory();

    useEffect(()=>{
        getUsers().then(handleUsers, handleError); 
    }, [users.length])

    useEffect(() => {
        filterUsers();
    }, [searchTerm]);

    
    const handleUsers = (users: UserEntity[]): void => {
        setPage(0);
        setUsers(users);
        setFilteredUsers(paginate(users, usersPerPage));
    };

    const handleError = (error: ErrorResponse): void => {
        if (error.status === 401){
            history.push("./login");
        } else {
            setErrorMessage(error.data.status);
        }
    };

    const sortUsers = (flattenedUsers: UserEntity[], sortField: string): UserEntity[] => {
        flattenedUsers.sort((a, b): number => {
            if (a[sortField] < b[sortField]) {
                return -1;
            }
            return 1;
        })
        return flattenedUsers;
    };

    const handleSort = (sortField: string): void => {
        let flattenedUsers = users.flat(1);
        let sortedUsers: UserEntity[] = sortUsers(flattenedUsers, sortField)
        setFilteredUsers(paginate(sortedUsers, usersPerPage));
    };

    //all searching related methods start here
    const hasTerm = (user: UserEntity): boolean => {
        if (user.firstName.toLowerCase().includes(searchTerm.toLowerCase())) {
            return true;
        }
        if (user.lastName.toLowerCase().includes(searchTerm.toLowerCase())) {
            return true;
        }
        if (user.title.toLowerCase().includes(searchTerm.toLowerCase())) {
            return true;
        }
        if (formatDate(user.createdAt).toLowerCase().includes(searchTerm.toLowerCase())) {
            return true;
        }
        return false;
    };

    const filterUsers = (): void => {
        let filtered:UserEntity[] = users.filter(user => hasTerm(user));
        let paginatedUsers = paginate(filtered, usersPerPage)
        if ( (page !== null) && (paginatedUsers.length -1 < page) ) {
            setPage(paginatedUsers.length-1);
        }
        setFilteredUsers(paginatedUsers);
    };

    const onSearchTermChange = () => (e: ChangeEvent<HTMLInputElement>): void => {
        setSearchTerm(e.target.value);
    };

    return (
        <div>
            <h2 id="users-header">EMPLOYEES</h2>
                { }
                <input
                    id="search-bar"
                    type="text"
                    placeholder="Search employees"
                    value={searchTerm}
                    onChange={onSearchTermChange()}
                />
            {auth.user && page !== null && 
                <UserTable users={filteredUsers[page]} handleSort={handleSort}/> 
            }
            <div className="button-bar">
                {auth.isAdmin()  && 
                    <Link to="/newUser">
                        <button className="add-new-employee">ADD NEW EMPLOYEE</button>
                    </Link>
                }
                {page !== null && 
                    <section className="pagination-buttons">
                        <button 
                            className="previous-next-button"
                            onClick={()=>setPage(page-1)}
                            disabled={page === 0}
                        >
                            Previous
                        </button>
                        {filteredUsers.map( (userPage, index) => {
                            return (
                                <button 
                                    className="pagination-button"
                                    onClick={()=>setPage(index)}
                                    type="button"
                                    disabled={page === index}
                                    key={`pagebutton${index}`}
                                >
                                    {index+1}
                                    </button>
                            );
                        })}
                        <button 
                            className="previous-next-button"
                            onClick={()=>setPage(page+1)}
                            disabled={page === filteredUsers.length-1}
                        >
                            Next
                        </button>
                    </section>
                }
            </div>
        </div>
     );
};
