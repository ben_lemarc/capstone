import React, { useState, useEffect, useContext, FC } from "react";
import { RouteComponentProps } from 'react-router';
import { createEmptyUser, UserEntity, userCanView, ErrorResponse } from '../model';
import { getUserView, updateUser } from '../api';
import { useParams } from 'react-router-dom';
import { UserView } from '../components';
import { AuthContext } from "../context/AuthContext";
import "../css/forms.css";

export const UserViewAuth: FC = () => {
    let { userId } = useParams();
    const [userView, setUserView] = useState<UserEntity|null>(null);
    const [viewError, setViewError] = useState<string|null>(null);
    const auth = useContext(AuthContext);

    useEffect(() =>{
        getUserView(userId).then(setUserView, setUserView);
    }, [userView && userView.id]);

    const getUserViewSuccessful = (user: UserEntity): void => {
        setUserView(user);
    };

    const getUserViewFailure = (error: ErrorResponse): void => {
        setViewError(error.data.status);
    };

    const updateSucceeded = (isValid: boolean): void => {
        if (isValid) {
            window.location.reload(false);
        } 
    };

    const handleUpdate = (updatedUser: UserEntity): void => {
        updateUser(updatedUser).then(updateSucceeded);
    };

    return (
        <>
            {userView && viewError === null && userCanView(userView.id, auth.user) 
                ? 
                <>
                    <p className="form-header">EDIT EMPLOYEE</p>
                    <UserView 
                        userBeingViewed={userView}
                        callOnSubmit={updateUser}
                    />
                </>
                :
                <h1>You are not authorized to view this content.</h1>       
            }
        </>
    );
}