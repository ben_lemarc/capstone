import React, { useState, useEffect, useContext, FC } from "react";
import { createEmptyUser, UserEntity } from "../model";
import { createUser } from "../api";
import { UserView } from "../components";
import { AuthContext } from "../context/AuthContext";
import "../css/forms.css";

export const NewUser: FC = () => {

    const auth = useContext(AuthContext);

    return (
        <>
            {!auth.isAdmin() ? 
                <h1>You are not authorized to view this content.</h1>
                :
                <>
                    <p className="form-header">ADD A NEW EMPLOYEE</p>
                    <UserView 
                        userBeingViewed={createEmptyUser()}
                        callOnSubmit={createUser}
                    />
                </>
            }
        </>
    );
};