import React, { useState, useEffect, useContext, FC } from 'react';
import { getArticles, getArticlesFromResponse } from "../api";
import { ArticleEntity } from "../model";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import "../css/article.css";
import {AuthContext} from "../context/AuthContext";
import { createEmptyUser, UserEntity, ErrorResponse } from "../model";
import { ArticleScroller, SubHeader, Trending } from "../components";

export const News: FC = () => {
    const [articles, setArticles] = useState<ArticleEntity[]>([]);
    const [trending, setTrending] = useState<ArticleEntity[]>([]);
    const [errorStatus, setErrorStatus] = useState<number>(0);
    const subItems: string[] = [
        "Sub Item one", 
        "Sub Item two", 
        "Sub Item three", 
        "Sub Item four has more ch..."
        ];
    const [subHeader, setSubHeader] = useState<string>(subItems[0]);

    const auth = useContext(AuthContext);

    useEffect(()=>{
        getArticles().then(handleArticles, handleError);
    }, [articles.length])

    const handleArticles = (articles: ArticleEntity[]): void => {
        setArticles(articles);
        getTrending();
    };

    const handleError = (error: ErrorResponse): void => {
        setErrorStatus(error.status);
    };

    const getTrending = (): void => {
        const trending = articles.filter(article =>  article.trending == true);
        setTrending(trending);
    };

    const changeSubheader = (newSubHeaderIndex: number): void => {
        setSubHeader(subItems[newSubHeaderIndex]);
    };

    const history = useHistory();

    if (errorStatus !== 0){
        history.push('./login');
    }

    return(
        <div className="article-grid">
            <SubHeader
                onChange={changeSubheader}
                subItems={subItems}
            />
            <ArticleScroller 
                articles={articles}
                subHeader={subHeader}
            />
            <Trending articles={trending}/>
        </div>
    );
}
