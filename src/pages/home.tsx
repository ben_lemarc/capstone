import React, { useState, useEffect, FC } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
import { getUserFromAPI, logout} from "../api";
import { useHistory } from "react-router-dom";
import { UserEntity, createEmptyUser } from "../model";


interface PropsForm {
    user: UserEntity;
}

export const Home: FC = () => {
    const history = useHistory();
    const handleLogout = () => {
        logout().then(() => history.push("/login"));
    };
    return (  
        <section>
            <button 
                onClick={handleLogout}
                className="Main Non-Submit"
            >
                Logout
            </button>
        </section>
    );
};


