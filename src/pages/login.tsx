import React, { useState, useContext, FC } from "react"
import { LoginEntity, createEmptyUser } from "../model";
import { getUserFromAPI } from "../api";
import {LoginComponent, AlreadySignedIn} from '../components'
import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import "../App.css"

export const Login: FC = () => {
    const auth = useContext(AuthContext);
    
    return (
        <>
            { auth.user ? <AlreadySignedIn/> : <LoginComponent/>}
        </>
    );
};