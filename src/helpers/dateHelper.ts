export const formatDate = (date: string): string => {
    const dateInfo: string[] = date.split("-");
    const year:string = dateInfo[0];
    const month: string = dateInfo[1];
    let day: string = "";
    if (dateInfo[2].includes('T')) 
        day = dateInfo[2].split('T')[0];
    else
        day = dateInfo[2].split(' ')[0];
    return `${month}/${day}/${year}`;
};