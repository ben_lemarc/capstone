import {UserEntity, createEmptyUser} from "../model";

export class StorageHelper {
    static USER_KEY = 'user';

    static getUser(): UserEntity|null{
        let authUserString = localStorage.getItem(this.USER_KEY);
        if (authUserString === null) {
            return null;
        }
        return JSON.parse(authUserString) as UserEntity;
    };

    static setUser(user: UserEntity|null): void {
        if (user === null) {
            localStorage.removeItem(this.USER_KEY);
        }
        else {
            localStorage.setItem(this.USER_KEY, JSON.stringify(user));
        }
    };
}