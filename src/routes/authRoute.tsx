import React, {useContext, FC} from "react"
import {Route, Redirect, RouteProps } from 'react-router-dom';
import { AuthContext } from "../context/AuthContext";

export const AuthRoute: FC<RouteProps> = ({children,...rest}) => {
        const auth = useContext(AuthContext);
        return (
            <Route
                {...rest}
                render={({ location }) =>
                auth.user ? (
                        children
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: location }
                            }}
                        />
                    )
                }
            />
        );
    };