import { ArticleEntity } from "./article"

export interface LoginErrorResponse {
  email?: string[];
  password?: string[];
}

export interface ErrorResponse {
  status: number;
  data: ErrorResponseData;
  statusText: string;
}

export interface ErrorResponseData {
	status:string;
  data: ErrorData;
}

export interface ErrorData {
  email: string[];
  first_name: string[];
  last_name: string[];
  phone_number: string[];
  message:string;
  [key: string]: string|string[];
}

export interface ArticlesResponse {
	data: ArticleRecord[];
}

export interface ArticleRecord {
  id: number;
  title: string;
  author: string;
  body: string;
  article_image_url: string;
  author_image_url: string;
  published_at: string;
  trending: boolean;
}

export interface UsersResponse {
	data: UserRecord[];
}

export interface UserRecord {
  first_name:string;
  last_name:string;
  title:string;
  email:string;
  id:number;
  role:string;
  created_at:string;
  phone_number:string;
  notes:string;
  full_time:boolean;
}

export interface LoggedInUser {
  id: number;
  role: string;
  first_name: string;
  last_name: string;
}

export interface KeyboardInputEvent {
  target: InputEventTarget;
}

export interface InputEventTarget {
  value: string;
  name: string;
}

