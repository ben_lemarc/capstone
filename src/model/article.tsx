export interface ArticleEntity {
  id: number;
  author: string;
  title: string;
  body: string;
  articleImageUrl: string;
  authorImageUrl: string;
  publishedAt: string;
  trending: boolean;
}

export const createEmptyArticle = (): ArticleEntity => ({
  id : -1,
  author: "",
  title: "",
  body: "",
  articleImageUrl: "",
  authorImageUrl: "",
  publishedAt: "",
  trending: false,
});