const DEVELOPER :string = "Developer";
const VP :string = 'VP';
const CEO :string = 'CEO';
const LEAD_DEVELOPER :string = 'Lead Developer';
const DESIGNER :string = 'Designer';

export const TITLES :string[] = [DEVELOPER, VP, CEO, LEAD_DEVELOPER, DESIGNER];

export const ADMIN :string = "admin";
const STANDARD :string = "standard";

export const ROLES :string[] = [ADMIN, STANDARD];

export interface UserEntity {
  firstName: string;
  lastName: string;
  title: string;
  email: string;
  id: number;
  role: string;
  createdAt: string;
  fullTime: boolean;
  notes: string;
  phoneNumber: string;
  [key: string]: string|boolean|number;
}

export const createEmptyUser = (): UserEntity => ({
  firstName: "",
  lastName: "",
  title: "Developer",
  email: "",
  id: -1,
  role: "standard",
  createdAt: "",
  fullTime: true,
  notes: "",
  phoneNumber: "",
});

export const userCanView = (id:number, loggedInUser:UserEntity|null):boolean => {
  if ( loggedInUser !== null && (  id === loggedInUser.id || loggedInUser.role === "admin")) {
  	  return true;
  }
  return false;
};

export const paginate = (users: UserEntity[], usersPerPage: number):UserEntity[][] => {
  const pages :number = Math.ceil(users.length/usersPerPage);
  let pageArray :UserEntity[][]=[];
  for ( let i = 0; i < pages - 1; i++){
    pageArray.push(users.slice(i*usersPerPage, (i+1)*usersPerPage));
  }
  pageArray.push(users.slice((pages-1)*usersPerPage, users.length));
  return pageArray;
};

export const getFullName = (user:UserEntity):string => {
  return `${user.firstName} ${user.lastName}`;
};