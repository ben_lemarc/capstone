import React, {FC} from "react";
import { Link } from "react-router-dom";
import "../css/footer.css"

export const Footer: FC = () => {
    const linkNumbers: number[] = [6, 3, 4, 6, 2];
    return (
        <section className="footer">
            <div className="link-flex">
                {[...Array(linkNumbers.length)].map((x, i) =>
                    <LinkList 
                        listIndex={i+1} 
                        linkNum={linkNumbers[i]}
                        key={`linkheader${i}`}
                    />
                )}
            </div>
            <p className="copyright-notice"> &#169; 2020 Woodridge Software </p>
        </section>
    );
};

interface LinkListProps {
    listIndex :number;
    linkNum : number;
} 

const LinkList: FC<LinkListProps>  = (props:LinkListProps) => {
    const { listIndex, linkNum } = props;
    return (
        <figure className="links">
            <figcaption className="list-heading">Footer Nav {listIndex}</figcaption>
            <ul className="link-list">
                {[...Array(linkNum)].map((x, i) =>
                    <li 
                        className="link-list"
                        key={`${listIndex}${i}`}
                        >
                        <a 
                            className="link-list" 
                            href="/#/home"
                        >
                            Item {listIndex}.{i+1}
                        </a>
                    </li>
                )}
            </ul>
        </figure>
    );
};
    
