import React, { useState, useContext, FC } from "react";
import { logout } from "../api";
import { useHistory } from "react-router-dom";
import {AuthContext, IAuthContext} from "../context/AuthContext";
import { createEmptyUser, ErrorResponse } from "../model";

export const AlreadySignedIn: FC = () => {
    const auth:IAuthContext = useContext(AuthContext);
    const [error, setError] = useState<string|null>(null);
    const history = useHistory();

    
    const handleLogout = () => {
        logout().then(logoutSuccessful, logoutUnsuccessful);
    };

    const logoutSuccessful = (success: boolean): void => {
        auth.setUser(null);
    };

    const logoutUnsuccessful = (error: ErrorResponse): void => {
        auth.setUser(null);
        setError(error.data.status);
    };

    return (
        <section>
            <p>You are already signed in</p>
            <button className="main non-submit" onClick={handleLogout}>logout</button>
            { error !== null && 
                    <p>An error occured. Please try again later</p> 
            }
        </section> 
    );
};