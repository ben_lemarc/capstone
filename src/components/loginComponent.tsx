import React, { useState, useEffect, useContext, ChangeEvent, FC } from "react"
import { LoginEntity, createEmptyLogin, UserEntity, ErrorResponse, LoginErrorResponse, KeyboardInputEvent } from "../model";
import { useHistory } from "react-router-dom";
import { isValidLogin, getUserFromAPI } from "../api";
import {AuthContext} from "../context/AuthContext";
import { TextInput } from "../components"
import "../css/forms.css"

export const LoginComponent: FC = () => {
    const [loginInfo, setLoginInfo] = React.useState<LoginEntity>(createEmptyLogin());
    const [errorMessage, setErrorMessage] = useState<string|null>(null)
    const auth = useContext(AuthContext);
    const history = useHistory();
    const FIRST_ERROR_MESSAGE = 0;

    const onTextFieldChange = (fieldId: string) => (e: ChangeEvent<HTMLInputElement>): void => {
        setLoginInfo({
            ...loginInfo,
            [fieldId]: e.target.value,
        });
    }; 

    const onTextInputChange = (event: KeyboardInputEvent) => {
        setLoginInfo({
            ...loginInfo,
            [event.target.name]: event.target.value,
        });
    };

    const loginSucceeded = (user: UserEntity): void => {
        auth.setUser(user);
    };

    const loginFailed = (error: LoginErrorResponse): void => {
        if (error.email !== undefined) {
            setErrorMessage(error.email[FIRST_ERROR_MESSAGE]);
        } 
        else if (error.password !== undefined) {
            setErrorMessage(error.password[FIRST_ERROR_MESSAGE]);
        }
    };

    const handleLogin = (login: LoginEntity): void => {
        isValidLogin(login).then(loginSucceeded, loginFailed);
    };

    return (
        <form>
            <h2>Login</h2>
            <TextInput
                propertyIdentifier="email"
                onInputChange={onTextInputChange}
                userProperty={loginInfo.email}
                label="Email"
                inputType="input"
            />
            <TextInput
                propertyIdentifier="password"
                onInputChange={onTextInputChange}
                userProperty={loginInfo.password}
                label="Password"
                inputType="input"
                isPassword={true}
            />
            { errorMessage !== null && 
                    <p className="error-message">{errorMessage}</p>
            }
            <button 
                type="button"
                className="main submit-button"
                onClick={() => handleLogin(loginInfo)}
            >
                Submit
            </button>
        </form>
    );
};