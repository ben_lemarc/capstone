import React, { FC } from "react";
import { ArticleEntity } from "../../model";
import { Article } from "./article";

interface ArticleScrollerProps {
    articles: ArticleEntity[];
    subHeader: string;
}

export const ArticleScroller: FC<ArticleScrollerProps> = ({articles, subHeader}: ArticleScrollerProps) => {
    
    return (
        <section className="article-section">
            <h4 
                className="div-header"
            >
                NEWS
                <span className="subheader-right-angle">
                    &gt;
                </span>
                <span className="subheader-title">
                    {subHeader}
                </span>
            </h4>
            {articles.map( (article) => 
                <Article 
                    article={article}
                    key={`article${article.id}`}
                />
            )}
        </section>
    );
};