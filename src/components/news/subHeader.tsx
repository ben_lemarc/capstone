import React, { FC } from "react";
import { SubHeaderButton } from "./subHeaderButton";


interface SubHeaderProps {
    onChange: (subHeaderIndex: number) => void
    subItems: string[];
}

export const SubHeader: FC<SubHeaderProps> = (props: SubHeaderProps) => {
    const {onChange, subItems} = props;  
    return (
        <section className="subheader">
            <h4 className="div-header">SUB HEADER</h4>
            <ul className="subheader">
                {subItems.map( (subItem: string, index: number) => 
                    <SubHeaderButton
                        subHeader={subItem}
                        onChange={onChange}
                        index={index}
                        key={subItem}
                        autoFocus={index === 0}
                    />
                )}
            </ul>
        </section>
    );
};