import React, { FC } from "react";
import { ArticleEntity } from "../../model";
import { formatDate } from "../../helpers/dateHelper";
import { TrendingArticle } from "./trendingArticle";

interface TrendingProps {
    articles: ArticleEntity[]
}

export const Trending: FC<TrendingProps> = (props: TrendingProps) => {
    const { articles } = props;
    return (
        <section className="sidebar">
            <h4 className="div-header">TRENDING</h4>
            { articles.map( (article) => 
                <TrendingArticle 
                    article={article} 
                    key={`trending${article.id}`}
                />) 
            }
        </section>
    );
};