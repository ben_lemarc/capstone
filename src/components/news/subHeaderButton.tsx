import React, { FC } from "react"; 

interface SubHeaderButtonProps {
    subHeader:string;
    onChange: (subHeader: number) => void
    index: number;
    autoFocus: boolean;
}

export const SubHeaderButton: FC<SubHeaderButtonProps> = (props: SubHeaderButtonProps) => {
    const {subHeader, onChange, index, autoFocus} = props;
    return (
        <button 
            className="subheader"
            type="button"
            onClick={()=>onChange(index)}
            autoFocus={autoFocus}
        >
            {subHeader}
        </button>
    );
};