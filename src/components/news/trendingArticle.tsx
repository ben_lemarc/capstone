import React, { FC } from "react";
import { ArticleEntity } from "../../model";
import { formatDate } from "../../helpers/dateHelper";

interface TrendingArticleProps {
    article: ArticleEntity;
}

export const TrendingArticle: FC<TrendingArticleProps> = (props: TrendingArticleProps) => {
    const { article } = props;
    return (
        <article className="trending-article">
            <img
                src={article.articleImageUrl}
                className="trending"
            />
            <span className="trending">{article.title}</span>
            <span className="published-at">{formatDate(article.publishedAt)}</span>
        </article>
    );
};