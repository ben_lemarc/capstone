import React, { FC } from "react";
import { ArticleEntity } from "../../model" 
import { formatDate } from "../../helpers/dateHelper";

interface ArticleProps {
    article: ArticleEntity;
}

export const Article: FC<ArticleProps> = ({article}: ArticleProps) => {
    return (
        <article className="article">
            <h2 className="article">{article.title}</h2>
            <div className="author-bio">
                <img 
                className="article-author"
                src={article.authorImageUrl}
                />
                <div className="name-and-date">
                    <span className="author-name">{article.author}</span>
                    <span className="published-at">{formatDate(article.publishedAt)}</span>
                </div>
            </div>
            <div className="article-text-and-image">
                <img 
                    className="article-image"
                    src={article.articleImageUrl}
                />
                <span className="article-text"> {article.body}</span>
            </div>
        </article>
    );
};