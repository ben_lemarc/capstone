import React, { FC } from "react";
import { KeyboardInputEvent } from "../../model"

export interface SelectOption {
	value: string;
	label: string;
}

interface SelectInputProps {
	label: string;
	options: string[];
	userProperty: string;
	property: string;
	onSelectChange: (event: KeyboardInputEvent) => void;
	disabled?: boolean;
}

export const SelectInput: FC<SelectInputProps> = ({label, options, userProperty, property, onSelectChange, disabled}:SelectInputProps) => {
	return (
        <div className="form-element">
            <label htmlFor="title">
                {label}
            </label>
            <select
                className="text-field"
                name={property}
                id={property}
                value={userProperty}
                onChange={onSelectChange}
                disabled={disabled}
            >
                {options.map( (option) => {
                    return (
                        <option 
                            value={option}
                            key={option}
                        >
                            {option}
                        </option>
                    )
                })}
            </select>
        </div>
	);
};