import React, { useContext, FC } from "react";
import { UserEntity, KeyboardInputEvent } from "../../model";
import { InputPicker } from "./inputPicker";

export interface TextInputProps {
    propertyIdentifier: string;
    onInputChange: (event: KeyboardInputEvent) => void
    userProperty: string;
    label: string;
    inputType: string;
    cssClass?: string;
    isPassword?: boolean;
};

export const TextInput:FC<TextInputProps> = (props:TextInputProps) => {
    let { propertyIdentifier, onInputChange, userProperty, label, inputType, cssClass, isPassword } = props;
    return (
        <div className="form-element">
                <label htmlFor={propertyIdentifier}>
                    {label}
                </label>
                <InputPicker 
                    propertyIdentifier={propertyIdentifier}
                    onInputChange={onInputChange}
                    userProperty={userProperty}
                    label={label}
                    inputType={inputType}
                    cssClass={cssClass}
                    isPassword={isPassword}
                />
        </div>
    );
};