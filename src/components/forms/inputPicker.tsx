import React, { useContext, FC } from "react";
import { UserEntity, KeyboardInputEvent } from "../../model"
import { TextInputProps } from "./textInput"

export const InputPicker: FC<TextInputProps> = (props:TextInputProps) => {
	let { propertyIdentifier, onInputChange, userProperty, label, inputType, cssClass, isPassword } = props;
	switch(inputType) {
		case "textarea":
			return (
                <textarea
                    className={ cssClass ? cssClass : "notes-area"}
                    name={propertyIdentifier}
                    id={propertyIdentifier} 
                    value={userProperty} 
                    onChange={onInputChange} 
                /> 
			);		
		default:
			return (
			    <input 
			    	className={ cssClass ? cssClass : "text-field"}
					type={ isPassword ? "password" : "text"} 
					name={propertyIdentifier}
					id={propertyIdentifier}
					value={userProperty} 
					onChange={onInputChange} 
					key={propertyIdentifier}
			    /> 
			);
	}
};