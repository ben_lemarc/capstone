import React, { useContext, FC } from "react";
import { KeyboardInputEvent } from "../../model"

interface RadioGroupProps {
	label: string;
	name: string;
	options: RadioButton[];
	onRadioChange: (event: KeyboardInputEvent) => void
}

export interface RadioButton {
	label: string;
	id: string;
	checked: boolean
}

export const createRadioButton = (label: string, id: string, checked:boolean): RadioButton => ({
    label : label,
    id : id,
    checked : checked
});

export const RadioGroup: FC<RadioGroupProps> = ({label, name, options, onRadioChange}:RadioGroupProps) => 
        <div className="form-element">
            <label>
                {label}
            </label>
            {options.map( (option: RadioButton) => {
            	return (
            		<div 
            			className="radio-button"
            			key={option.id}
        			>
		                <input
		                    type="radio" 
		                    name={name} 
		                    id={option.id}
		                    value={option.id}
		                    checked={option.checked}
		                    onChange={onRadioChange}
		                />
		                <label htmlFor={option.id}>
		                    {option.label}
		                </label>
		            </div>
            	);
            })}
        </div>