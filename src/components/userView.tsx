import React, { useState, useEffect, useContext, FC, KeyboardEvent, ChangeEvent } from "react";
import { UserEntity, TITLES, ROLES, ErrorResponse, KeyboardInputEvent } from "../model";
import { AuthContext } from "../context/AuthContext";
import { useHistory } from "react-router-dom";
import { TextInput, SelectInput, RadioButton, createRadioButton, RadioGroup } from "./forms";
import "../css/forms.css"; 

interface Props {
    userBeingViewed: UserEntity;
    callOnSubmit: (user: UserEntity) => Promise<boolean>;
}

export const UserView: FC<Props> = (props: Props) => {
    const auth = useContext(AuthContext);
    const { userBeingViewed, callOnSubmit } = props; 
    const [userInfo, setUserInfo] = useState<UserEntity>(userBeingViewed);
    const [errorStatus, setErrorStatus] = useState<number|null>(null);
    const [errorMessages, setErrorMessages] = useState<string[]>([]);
    const [workCapacityButtons, setWorkCapacityButtons] = useState<RadioButton[]>([])
    const history = useHistory();
    const FIRST_ERROR_MESSAGE = 0;

    useEffect(()=> {
        setUserInfo(userBeingViewed);
    }, [userBeingViewed.id]);

    useEffect(() => {
        setWorkCapacityButtons([
            createRadioButton("Full Time", "true", userInfo.fullTime),
            createRadioButton("Part Time", "false", !userInfo.fullTime),
        ])
    }, [userInfo.fullTime])

    const submissionSucceeded = (): void => {
        history.push('/home');
    };

    const submissionFailed = (response:ErrorResponse): void => {
        setErrorStatus(response.status);
        if (response.status === 422){
            const errors :string[] = Object.keys(response.data.data).map( (key, i) => 
                response.data.data[key][FIRST_ERROR_MESSAGE]
            )  
            setErrorMessages(errors);
        }   
        else {
            setErrorMessages([response.data.status]);
        }
    };

    const handleSubmission = (newUser: UserEntity): void => {
        callOnSubmit(newUser).then(submissionSucceeded, submissionFailed);
    };

    const onRadioChange = (): void => {
        setUserInfo({
            ...userInfo,
            fullTime: !userInfo.fullTime,
        });
    };

    interface ErrorMessageProps {
        errorStatus: number;
        errorMessages: string[];
    }

    const ErrorMessageComponent: FC<ErrorMessageProps> = ({errorStatus, errorMessages}: ErrorMessageProps) => 
            (
                <>
                    {errorMessages.map ((errorMsg) => 
                            <p className="error-message">{errorMsg}</p>
                        )
                    }
                </>
            );

    const onTextInputChange = (event: KeyboardInputEvent) => {
        setUserInfo({
            ...userInfo,
            [event.target.name]: event.target.value,
        });
    };

    return (
        <section>
            <form>
                <TextInput
                    propertyIdentifier="firstName"
                    onInputChange={onTextInputChange}
                    userProperty={userInfo.firstName}
                    label="First Name"
                    inputType="input"
                />
                <TextInput
                    propertyIdentifier="lastName"
                    onInputChange={onTextInputChange}
                    userProperty={userInfo.lastName}
                    label="Last Name"
                    inputType="input"
                />
                <SelectInput
                    label="Title"
                    options={TITLES}
                    userProperty={userInfo.title}
                    property="title"
                    onSelectChange={onTextInputChange}
                />
                <RadioGroup
                    label="Work Capacity"
                    name="fullTime"
                    options={workCapacityButtons}
                    onRadioChange={onRadioChange}
                />
                <TextInput
                    propertyIdentifier="notes"
                    onInputChange={onTextInputChange}
                    userProperty={userInfo.notes}
                    label={"Notes & comments"}
                    inputType="textarea"
                />
                <TextInput
                    propertyIdentifier="phoneNumber"
                    onInputChange={onTextInputChange}
                    userProperty={userInfo.phoneNumber}
                    label="Mobile"
                    inputType="input"
                    cssClass="phone-field"
                />
                <TextInput
                    propertyIdentifier="email"
                    onInputChange={onTextInputChange}
                    userProperty={userInfo.email}
                    label="Personal Email"
                    inputType="input"
                />
                <SelectInput
                    label="Role"
                    userProperty={userInfo.role}
                    property="role"
                    options={ROLES}
                    onSelectChange={onTextInputChange}
                    disabled={!auth.isAdmin()}
                />
                {errorStatus !== null &&
                    <ErrorMessageComponent 
                        errorStatus={errorStatus} 
                        errorMessages={errorMessages}/> 
                }
                <div className="form-submitter">
                    <button 
                        type="button"
                        className="main submit-button"
                        onClick={()=>handleSubmission(userInfo)}
                    >
                        Save
                    </button>
                    <div className="notify">
                        <input type="checkbox" id="notify" name="notify" value="notify"/>
                        <label htmlFor="notify">Notify Employee</label>
                    </div>
                </div>
            </form>     
        </section>
    );
};