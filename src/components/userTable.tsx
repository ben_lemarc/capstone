import React, { useState, useEffect, useContext, FC } from "react"
import { UserEntity, userCanView, ErrorResponse } from "../model";
import { deleteUser } from "../api";
import { Link } from "react-router-dom";
import { formatDate } from "../helpers/dateHelper";
import { AuthContext } from "../context/AuthContext";
import "../css/userTable.css";
import "../App.css";

interface ListProps {
    users:UserEntity[];
    handleSort: (sortField:string) => void;
}

const FIRST_NAME_LABEL: string = "First Name";
const LAST_NAME_LABEL: string = "Last Name";
const TITLE_LABEL: string = "Title";
const DATE_ADDED_LABEL: string = "Date Added";
const TABLE_COLUMNS: string[] = [FIRST_NAME_LABEL, LAST_NAME_LABEL, TITLE_LABEL, DATE_ADDED_LABEL];

const FIRST_NAME_FIELD: string = "firstName";
const LAST_NAME_FIELD: string = "lastName";
const TITLE_FIELD: string = "title";
const DATE_ADDED_FIELD: string = "createdAt";
const SORT_FIELDS: string[] = [FIRST_NAME_FIELD, LAST_NAME_FIELD, TITLE_FIELD, DATE_ADDED_FIELD];

export const UserTable:FC<ListProps> = ({users, handleSort}:ListProps) => {

    const auth = useContext(AuthContext);
    const [error, setError] = useState<string|null>(null)

    interface LinkProps {
        userId:number;
    }

    const handleDelete = (userId: number): void => {
        deleteUser(userId).then(deleteSuccessful, deleteError);
    };

    const deleteSuccessful = (): void => {
        window.location.reload();
    };

    const deleteError = (error: ErrorResponse): void => {
        setError(error.data.status);
    };

    const TableLinks: FC<LinkProps> = ({userId}: LinkProps) => {
        return (
            <>
                <Link className="main" to={`/users/${userId}`}>Edit</Link>
                <span className="link-divider">|</span>
                <button 
                    className="delete-user" 
                    onClick={()=>handleDelete(userId)}
                >
                    Delete
                </button>
            </>
        );
    };

    return (
        <section>
            <table>
                <thead>
                    <tr className="heading">
                        {TABLE_COLUMNS.map( (column: string, i: number) => 
                            <th key={`column${column}`} >
                                <button 
                                    onClick={()=>handleSort(SORT_FIELDS[i])}
                                    className="bolded-text sort-table"
                                    id={column}
                                >
                                    {column}
                                </button>
                            </th>
                        )}
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {users.map( (user) => 
                        <tr key={`row${user.id}`} >
                            {SORT_FIELDS.map((field: string)=> 
                                <td key={`${user.id}${field}`}>
                                    {field !== DATE_ADDED_FIELD 
                                            ? user[field]
                                            : formatDate(user["createdAt"])
                                    }
                                </td> 
                            )}
                            <td 
                                className="link-column"
                                key={`${user.id}mod`}>
                                {userCanView(user.id, auth.user) &&
                                    <TableLinks userId={user.id} />
                                }
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </section>
    );
};