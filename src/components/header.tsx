import React, { useState, useEffect, useContext, FC } from "react";
import  '../css/header.css'
import { useLocation } from 'react-router-dom'
import logo from "../woodridgeLogo.png"
import { AuthContext, IAuthContext} from "../context/AuthContext";
import { getFullName } from "../model";
import { Link } from "react-router-dom";

export const Header: FC = () => {

    const auth: IAuthContext = useContext(AuthContext);
    const location = useLocation();

    const headerLinks: HeaderLinkProps[] = [
        createHeaderLinkProps("/home", "Home", location.pathname === "/home"),
        createHeaderLinkProps("/news","News", location.pathname === "/news"),
        createHeaderLinkProps("/news","Nav item Three", false),
        createHeaderLinkProps("/news","Nav item Four", false),
        createHeaderLinkProps("/news","Nav item Five", false),
    ]

    return (
        <section className="header-section">
            <div id="header-grid">
                <img id="header-image-grid" src={logo} alt="logo"/>
                <h1 id="header-heading-grid">TRAINING</h1>
                <Link 
                    to="/login"
                    id="header-grid-login"
                >
                    {auth.user ? getFullName(auth.user) : "Login"}
                </Link>
            </div>
            <div className="header-links-bar">
                {headerLinks.map( (link) => 
                    <HeaderLink
                        onPage={ link.onPage }
                        url={link.url}
                        label={link.label}
                        key={link.label}
                    />
                )}
            </div>
        </section>
    );
};

interface HeaderLinkProps {
    url: string;
    label: string;
    onPage: boolean;
}

const createHeaderLinkProps = (url: string, label: string, onPage: boolean): HeaderLinkProps => ({
    url: url,
    label: label,
    onPage: onPage,
});

const HeaderLink: FC<HeaderLinkProps> = ({url, label, onPage}: HeaderLinkProps) => 
    (
        <Link 
            to={url}
            id={label}
            className={onPage ? "selected-header-link" : "header-links"}
        >
            {label}
        </Link>
    );

        
